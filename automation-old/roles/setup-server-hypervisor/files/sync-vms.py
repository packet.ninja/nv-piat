#!/usr/bin/python3

import os
import yaml
from pprint import pprint

def main():
    stream = os.popen('ip netns list | awk \'{print $1}\'')
    currentns = stream.read().splitlines()

    stream = open("/etc/network/vms.yaml", 'r')
    vms = yaml.load(stream)

    syncvm(vms,currentns)

def split(word):
    return [char for char in word]

def syncvm(vms,currentns):
    if vms is not None:
        for vm in vms:
            if vm not in currentns:
                createvm(vms,vm)
    if currentns is not None:
        for ns in currentns:
            if vms is not None:
                if ns not in vms:
                    removevm(currentns,ns)
            else:
                removevm(currentns,ns)

def createvm(vms,vm):
    print("creating " + vm + "\n")

    os.system('ip netns add ' + vm)
    os.system('ip link add ' + vm + 'lhv type veth peer name ' + vm + 'lvm')
    os.system('ip link set ' + vm + 'lhv mtu 9000')
    os.system('ip link set ' + vm + 'lvm mtu 9000')
    if len(vms[vm]["id"]) < 3:
      os.system('ip link set ' + vm + 'lhv address 44:38:39:EE:00:' + vms[vm]["id"])
      os.system('ip link set ' + vm + 'lvm address 44:38:39:FF:00:' + vms[vm]["id"])
    else:
      post = split(vms[vm]["id"])
      os.system('ip link set ' + vm + 'lhv address 44:38:39:EE:0' + post[0] + ':' + post[1] + post[2])
      os.system('ip link set ' + vm + 'lvm address 44:38:39:FF:0' + post[0] + ':' + post[1] + post[2])
    os.system('ip link set ' + vm + 'lvm netns ' + vm)
    os.system('ip link add ' + vm + 'mhv type veth peer name ' + vm + 'mvm')
    os.system('ip link set ' + vm + 'mhv mtu 9000')
    os.system('ip link set ' + vm + 'mvm mtu 9000')
    if len(vms[vm]["id"]) < 3:
      os.system('ip link set ' + vm + 'mhv address 44:38:39:AA:00:' + vms[vm]["id"])
      os.system('ip link set ' + vm + 'mvm address 44:38:39:BB:00:' + vms[vm]["id"])
    else:
      post = split(vms[vm]["id"])
      os.system('ip link set ' + vm + 'mhv address 44:38:39:AA:0' + post[0] + ':' + post[1] + post[2])
      os.system('ip link set ' + vm + 'mvm address 44:38:39:BB:0' + post[0] + ':' + post[1] + post[2])
    os.system('ip link set ' + vm + 'mvm netns ' + vm)

    os.system('ip link set dev ' + vm + 'lhv master bridge')
    os.system('bridge vlan del vid 1 dev ' + vm + 'lhv')
    os.system('bridge vlan del vid 1 untagged pvid dev ' + vm + 'lhv')
    os.system('bridge vlan add vid ' + vms[vm]["vlan"] + ' dev ' + vm + 'lhv')
    os.system('bridge vlan add vid ' + vms[vm]["vlan"] + ' untagged pvid dev ' + vm + 'lhv')
    os.system('ip link set dev ' + vm + 'mhv master bridge')
    os.system('bridge vlan del vid 1 dev ' + vm + 'mhv')
    os.system('bridge vlan del vid 1 untagged pvid dev ' + vm + 'mhv')
    os.system('bridge vlan add vid 10 dev ' + vm + 'mhv')
    os.system('bridge vlan add vid 10 untagged pvid dev ' + vm + 'mhv')

    os.system('ip link set ' + vm + 'lhv up')
    os.system('ip link set ' + vm + 'mhv up')
    if len(vms[vm]["id"]) < 3:
      os.system('bridge fdb del 44:38:39:FF:00:' + vms[vm]["id"] + ' dev uplink master vlan ' + vms[vm]["vlan"])
      os.system('bridge fdb add 44:38:39:FF:00:' + vms[vm]["id"] + ' dev ' + vm + 'lhv master vlan ' + vms[vm]["vlan"] + ' static sticky')
    else:
      post = split(vms[vm]["id"])
      os.system('bridge fdb del 44:38:39:FF:0' + post[0] + ':' + post[1] + post[2] + ' dev uplink master vlan ' + vms[vm]["vlan"])
      os.system('bridge fdb add 44:38:39:FF:0' + post[0] + ':' + post[1] + post[2] + ' dev ' + vm + 'lhv master vlan ' + vms[vm]["vlan"] + ' static sticky')

    os.system('ip netns exec ' + vm + ' ip link set ' + vm + 'lvm up')
    os.system('ip netns exec ' + vm + ' ip link set ' + vm + 'mvm up')

    os.system('ip netns exec ' + vm + ' /sbin/dhclient -nw -pf /run/dhclient-lvm-' + vm + '.pid ' + '-lf /var/lib/dhcp/dhclient.' + vm + 'lvm.leases ' + vm + 'lvm')
    os.system('ip netns exec ' + vm + ' /sbin/dhclient -nw -pf /run/dhclient-mvm-' + vm + '.pid ' + '-lf /var/lib/dhcp/dhclient.' + vm + 'mvm.leases ' + vm + 'mvm')
    os.system('ip netns exec ' + vm + ' /usr/sbin/sshd -o PidFile=/run/sshd-' + vm + '.pid')


def removevm(currentns,ns):
    print("deleting " + ns + "\n")

    os.system('pkill -F /run/dhclient-lvm-' + ns + '.pid')
    os.system('pkill -F /run/dhclient-mvm-' + ns + '.pid')
    os.system('pkill -F /run/sshd-' + ns + '.pid')
    os.system('ip link del ' + ns + 'lhv')
    os.system('ip link del ' + ns + 'mhv')
    os.system('ip netns del ' + ns)

main()
