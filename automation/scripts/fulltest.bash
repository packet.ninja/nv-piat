#!/bin/bash

echo "[+] - Underlay"
netq check bgp|grep Total
#ansible leaf -m shell -a "net show bgp evpn route|grep Display"

echo "==== check racks inside site01 - pod1 ===="
echo "[+] - rack01 <=> rack02 (L3VNI)"
ansible server01 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.2.103 > /dev/null"
echo "[+] - rack01 <=> rack03 (L3VNI)"
ansible server01 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.3.105 > /dev/null"

echo "==== check PODs insde site01 ===="
echo "[+] - rack01(pod1) <=> rack04(pod2) (L3VNI)"
echo "server01 <===> server07"
ansible server01 -m shell -b -a "ip vrf exec default ping -q -c3 10.112.4.107 > /dev/null"
echo "server03 <===> server09"
ansible server03 -m shell -b -a "ip vrf exec default ping -q -c3 10.112.5.109 > /dev/null"
echo "server05 <===> server11"
ansible server05 -m shell -b -a "ip vrf exec default ping -q -c3 10.112.6.111 > /dev/null"


echo "==== DCI ===="
echo "[+] - DCI L2VNI - site01(pod1) <=> site02"
echo "server01 <===> server15"
ansible server01 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.1.115 > /dev/null"
echo "server03 <===> server17"
ansible server03 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.2.117 > /dev/null"
echo "server05 <===> server19"
ansible server05 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.3.119 > /dev/null"

echo "[+] - DCI L3VNI - site01(pod1) <=> site02"
echo "server05 <===> server15"
ansible server05 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.1.115 > /dev/null"
echo "server03 <===> server17"
ansible server03 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.2.117 > /dev/null"
echo "server01 <===> server19"
ansible server01 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.3.119 > /dev/null"


echo "[+] - DCI L3VNI - intraPOD | site01-pod2 <=> site02-pod1"
echo "server07 <===> server15"
ansible server07 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.1.115 > /dev/null"
echo "server09 <===> server17"
ansible server09 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.2.117 > /dev/null"
echo "server11 <===> server19"
ansible server11 -m shell -b -a "ip vrf exec default ping -q -c3 10.200.3.119 > /dev/null"


