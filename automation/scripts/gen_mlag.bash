#!/bin/bash

echo -n "vlan: " ; read -r vlan
echo -n "loopback1: " ; read -r loopback1
echo -n "MLAG MAC (ex: 44:38:39:BE:EF:34): " ; read -r mlag_mac
echo -n "MLAG vxlan shared ip: " ; read -r mlag_shared_ip
echo -n "vlan$vlan ip1 (no CIDR): " ; read -r vlan_ip1
echo -n "vlan$vlan VRR IP (no CIDR): " ; read -r vlan_ip_vrr
echo -n "vlan$vlan VRR MAC (ex: 00:00:5e:00:01:00): " ; read -r vrr_mac

loopback2=${loopback1%.*}.$((${loopback1##*.}+1))
vlan_ip2=${vlan_ip1%.*}.$((${vlan_ip1##*.}+1))

if [ -z "$vrr_mac" ]; then 
	vrr_mac="00:00:5e:00:01:00"
fi

echo "
############################################################################
# NODE 1
############################################################################

# VxLAN mapping
nv set bridge domain br_default vlan $vlan vni 100${vlan}

# MLAG
nv set mlag backup $loopback2
nv set mlag enable on
nv set mlag init-delay 2
nv set mlag mac-address $mlag_mac

# VxLAN
nv set nve vxlan enable on
nv set evpn enable on
nv set nve vxlan mlag shared-address $mlag_shared_ip
nv set nve vxlan source address $loopback1

# Bonding
nv set interface bond1 bond member swp1
nv set interface bond1 bond mlag id 1
nv set interface bond1 type bond
nv set interface bond1 bridge domain br_default access $vlan
nv set interface bond1 bond mlag enable on

# ISL
nv set interface peerlink bond member swp49
nv set interface peerlink bond member swp50
nv set interface peerlink type peerlink
nv set interface peerlink.4094 base-interface peerlink
nv set interface peerlink.4094 type sub
nv set interface peerlink.4094 vlan 4094

# Interfaces
nv set interface swp1,49-50,51,52,53,54 link state up
nv set interface swp1,49-50,51,52,53,54 type swp

# VLAN
nv set interface vlan$vlan ip address $vlan_ip1/24
nv set interface vlan$vlan ip vrr address $vlan_ip_vrr/24
nv set interface vlan$vlan vlan $vlan
nv set interface vlan$vlan ip vrf tenant1
nv set interface vlan$vlan ip vrr enable on
nv set interface vlan$vlan ip vrr mac-address $vrr_mac 
nv set interface vlan$vlan ip vrr state up
nv set interface vlan$vlan type svi

# L3VNI
nv set vrf tenant1 evpn vni 400${vlan}

############################################################################
#  NODE 2
############################################################################
# VxLAN mapping
nv set bridge domain br_default vlan $vlan vni 100${vlan}

# MLAG
nv set mlag backup $loopback1
nv set mlag enable on
nv set mlag init-delay 2
nv set mlag mac-address $mlag_mac

# VxLAN
nv set nve vxlan enable on
nv set evpn enable on
nv set nve vxlan mlag shared-address $mlag_shared_ip
nv set nve vxlan source address $loopback2

# Bonding
nv set interface bond1 bond member swp1
nv set interface bond1 bond mlag id 1
nv set interface bond1 type bond
nv set interface bond1 bridge domain br_default access $vlan
nv set interface bond1 bond mlag enable on

# ISL
nv set interface peerlink bond member swp49
nv set interface peerlink bond member swp50
nv set interface peerlink type peerlink
nv set interface peerlink.4094 base-interface peerlink
nv set interface peerlink.4094 type sub
nv set interface peerlink.4094 vlan 4094

# Interfaces
nv set interface swp1,49-50,51,52,53,54 link state up
nv set interface swp1,49-50,51,52,53,54 type swp

# VLAN
nv set interface vlan$vlan ip address $vlan_ip2/24
nv set interface vlan$vlan ip vrr address $vlan_ip_vrr/24
nv set interface vlan$vlan vlan $vlan
nv set interface vlan$vlan ip vrf tenant1
nv set interface vlan$vlan ip vrr enable on
nv set interface vlan$vlan ip vrr mac-address $vrr_mac 
nv set interface vlan$vlan ip vrr state up
nv set interface vlan$vlan type svi

# L3VNI
nv set vrf tenant1 evpn vni 400${vlan}

"


exit
