# Testing plan
## IPAM
- site01-pod1: 10.111.x.x
- site01-pod2: 10.112.x.x
- site02: 10.121.x.x
- Any stretched prefixes: 10.200.x.x 
## Loopback
- 10.10.0.0/16 - site01
- 10.10.1.0/24 - site01-pod1
- 10.10.2.0/24 - site01-pod2
- 10.20.0.0/16 - site02

## Servers
### site01-pod1
- server01 - 10.200.1.101       vlan2001/vni102001
- server03-  10.200.2.103       vlan2002/vni102002  
- server05 - 10.200.3.105       vlan2003/vni102003
### site01-pod2
- server07 - 10.112.4.107       vlan1004/vni101004 
- server09 - 10.112.5.109       vlan1005/vni101005
- server11 - 10.112.6.111       vlan1006/vni101006
### site02
- server15 - 10.200.1.115       vlan2001/vni102001
- server17 - 10.200.2.117       vlan2002/vni102002
- server19 - 10.200.3.119       vlan2003/vni102003

### Fabric
```
        loopback    mlag-shared vlan/vxlan clag-sysmac          IP          L3VNI   Tenant
==== site01 - pod1 ===
leaf01  10.10.1.1   --          2001/102001                     10.200.1.11 400100  tenant1
leaf02  10.10.1.2   --          2001/102001                     10.200.1.12 400100  tenant1
leaf03  10.10.1.3   10.10.1.34  2002/102002 44:38:39:BE:EF:34   10.200.2.3  400100  tenant1
leaf04  10.10.1.4   10.10.1.34  2002/102002 44:38:39:BE:EF:34   10.200.2.4  400100  tenant1
leaf05  10.10.1.5               2003/102003                     10.200.3.5  400100  tenant1          
leaf06  10.10.1.6               2003/102003                     10.200.3.6  400100  tenant1
==== site01 - pod2 ===
leaf07  10.10.2.7               1004/101004                     10.112.4.7  400100  tenant1
leaf08  10.10.2.8               1004/101004                     10.112.4.8  400100  tenant1
leaf09  10.10.2.9   10.10.2.91  1005/101005 44:38:39:BE:EF:91   10.112.5.9  400100  tenant1
leaf10  10.10.2.10  10.10.2.91  1005/101005 44:38:39:BE:EF:91   10.112.5.10 400100  tenant1
leaf11  10.10.2.11              1006/101006                     10.112.6.11 400100  tenant1
leaf12  10.10.2.12              1006/101006                     10.112.6.12 400100  tenant1
==== site02 ===
leaf13  10.20.1.13              2001/102001                     10.200.1.13 400100  tenant1
leaf14  10.20.1.14              2001/102001                     10.200.1.14 400100  tenant1
leaf15  10.20.1.15  10.20.1.56  2002/102002 44:38:39:BE:EF:56   10.200.2.15 400100  tenant1
leaf16  10.20.1.16  10.20.1.56  2002/102002 44:38:39:BE:EF:56   10.200.2.16 400100  tenant1
leaf17  10.20.1.17              2003/103001                     10.200.3.17 400100  tenant1
leaf18  10.20.1.18              2003/103001                     10.200.3.18 400100  tenant1
```

# Save & restore
`save.sh` Save configuration to yml and `commands` from all devices in `snippets`

`ansible-playbook playbooks/save.yml -l leaf01` for a specific device

`restore.sh` to restore all the configurations from `snippets`

`ansible-playbook playbooks/restore.yml -l leaf01` for a specific device
 
# Features testing
#### ATTILLA ####
## BGP (both IPv4 & IPv6)
- `net show bgp`
- `net show bgp sum`

## Static IPv4 routes (with or without next-hop definition)
- `net show route ipv4`

## Static IPv6 routes (with Link Local address as next-hop)
- `net show route ipv6`

## ECMP multipath
- From spine01: `net show bgp` look at `=`
- `net show route` - montrer ipv6 nexthop & interface
- `net show route 10.10.10.111`

## VxLAN L2 Static Ingress Replication
- Leaf01: Type of BUM mechanism: `net show bgp evpn vni`
- Leaf01: List of VNI locally: `net show evpn vni`
- leaf01: List of VNI in the fabric: ` net show evpn vni 102001` (show remote vtep)
## EVPN L3VNI
- `net show evpn vni`
- `net show evpn rmac vni all`

## inter-VRFs routes leaking
TODO

#### JULIEN ####
## LACP
From `leaf01`
- `net show interface bonds`
- `net show interface bondmem`

## SVIs & VRFs
- `net show vrf`
- `net show vrf list`
- `net show vrf vni`
- `net show interfaces` (talk about LLDP)

## MLAG
### Shut port down link
- `net show clag` - show loopback/anycast / Clag ID
- `net show clag verbose` verify timers
- `net show bgp sum` + ifquery lo
- From server01, ping server03: `sudo ip vrf exec default ping 10.200.2.103`
- From server03, `ifquery uplink` `cat /proc/net/bonding/uplink` - `sudo tcpdump -i uplink icmp`
- Open 4 terms for leaves03/04
- Show connectivy from leaf03/leaf04: `net show interface` + swp1 + bond1
- From leaf03 and leaf04: `sudo tcpdump -ni swp1 icmp`
- Open other shell to leaf03/leaf04, shut interface towards server01: `sudo ifdown swp1`
- Verify traffic and stop ping to verify the amount of packet lost
- Re-up interface from leave `sudo ifdown swp1`
- Show `net show clag` - reup the remote interface
### reboot
- Verify tcpdump + new ping are running
- `sudo reboot` the leave where traffic is flowing
- Show `net show clag` - backup inactive 

### Multihoming
- Between server01 / server19
- Show map show 
- start ping from `server19` - 
- Check bond on leaf01/leaf02: 
- `ifquery bond1`
- `net show interface bond1` - show UP and EVPN-MH and ARP-ND
- Check status: `net show evpn vni` - `net show bgp evpn vni` - `net show evpn`
- Check ES: `net show evpn es` - NOTE THE FLAGS
- Check ES-EVI local: `net show evpn es-evi` 
- Check DF `net show evpn es detail|grep DF` - check priority and DF
- Look at the fabric from `exit01`:
- Check route type 1 and 4
- `net show bgp evpn route type 4` - check pref
- `net show bgp evpn route type 4|grep Distin` 
- `net show bgp evpn route rd 10.10.1.1 type 4:` - check hostname - check Best
- DEMO SHUT INTERFACE
- Open 4 terms on leaves,
- `net show evpn es`
- `tcpdump` on leaves
- leaf01: `sudo ifdown swp1` 
- leaf01: `net show interface|grep bond1`
- leaf01/02: `net show evpn es` - Comment on the flags and remote VTEP
- leaf01: `sudo ifup swp1` - count the ping
- leaf01/02: `net show evpn es` - Comment on the flags and remote VTEP
- check route somewhere else
- SAME TEST but check on spine08 (nothing to do with that): `net show bgp evpn route type 1` + grep paths

- DEMO reboot
- ping from `server19`
- restart ping for counting packet loss
- tcpdump everywhere


- Show ethernet segment per VNI: net show bgp l2vpn evpn es
- Show BGP Ethernet Segment Information and per VNI: net show bgp l2vpn evpn es-evi
- Show EAD type 1: net show bgp l2vpn evpn route type ead


######################## NOT WORKING ON VX ########################

## QinVNI
Plan is to transport the customer’s VLAN tag over the fabric, via VNI => That should work

## IPv4 & IPv6 ACLs (with IP Src, IP Dest, Protocol & VLAN)
OK

## Up to 96x different QoS policies, for rate-limiting purpose
We don’t have tested/validated numbers

## uRPF
We don’t support this feature.
## Storm-Control for Broadcast & Multicast traffic
- Start broadcast packet flows
    - `sudo ping -f -b 192.23.45.255 -I 192.23.45.22`
- Manually set storm control via (replace $IFACE with swpX for test port): 
    - `sudo echo 100 > /cumulus/switchd/config/interface/$IFACE/storm_control/broadcast`
- Verify broadcast pps is clipped on remote test node, counters, and/or pcap
    - `net clear counters ; net show counters`

######################## NOT WORKING ON VX ########################

# Walkthrough
## Server connectivity
```
ping from server01
# net show neighbor
# net show lldp
# net show bridge macs

net show evpn
net show evpn vni
net show evpn vni detail # VTEP
net show evpn vni 10 # verify VNI / VLAN / VRF
```
## VxLAN
```
# net show evpn vni
# net show evpn arp-cache vni 10
# net show bgp l2vpn evpn vni
# net show bgp l2vpn evpn route rd 10.10.10.1:3
2 types of type 2 route: MAC and MAP/IP
```

## VxLAN routing
```
# net show route vrf tenant1
# net show evpn vni 4001
```



# Debug
```
alias az='nv config apply -y'

v list-commands
nv config apply
nv config save
nv config detach

- nv config diff 
`list files`
nv config history
nv config show

# erase
nv config apply empty

sudo rm -rf /etc/nvue.d/*.yaml
nv config apply startup

# debug
sudo journalctl -u nvued -f &
cat /etc/default/nvued
sudo systemctl restart nvued
```
