nv set router bgp autonomous-system 4202020100
nv set router bgp enable on
nv set router bgp router-id 10.20.11.11
nv set router policy route-map advertiseloopback rule 10 action permit
nv set router policy route-map advertiseloopback rule 10 match interface lo
nv set system hostname spine11
nv set vrf default router bgp address-family ipv4-unicast enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected route-map advertiseloopback
nv set vrf default router bgp address-family l2vpn-evpn enable on
nv set vrf default router bgp enable on
nv set vrf default router bgp neighbor swp1 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp1 remote-as external
nv set vrf default router bgp neighbor swp1 type unnumbered
nv set vrf default router bgp neighbor swp2 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp2 remote-as external
nv set vrf default router bgp neighbor swp2 type unnumbered
nv set vrf default router bgp neighbor swp3 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp3 remote-as external
nv set vrf default router bgp neighbor swp3 type unnumbered
nv set vrf default router bgp neighbor swp4 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp4 remote-as external
nv set vrf default router bgp neighbor swp4 type unnumbered
nv set vrf default router bgp neighbor swp5 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp5 remote-as external
nv set vrf default router bgp neighbor swp5 type unnumbered
nv set vrf default router bgp neighbor swp6 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp6 remote-as external
nv set vrf default router bgp neighbor swp6 type unnumbered
nv set vrf default router bgp neighbor swp61 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp61 remote-as external
nv set vrf default router bgp neighbor swp61 type unnumbered
nv set vrf default router bgp neighbor swp62 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp62 remote-as external
nv set vrf default router bgp neighbor swp62 type unnumbered
nv set vrf default router bgp neighbor swp63 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp63 remote-as external
nv set vrf default router bgp neighbor swp63 type unnumbered
nv set vrf default router bgp neighbor swp64 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp64 remote-as external
nv set vrf default router bgp neighbor swp64 type unnumbered
nv set vrf default router bgp path-selection multipath aspath-ignore on
nv set interface eth0 ip address dhcp
nv set interface eth0 ip vrf mgmt
nv set interface eth0 type eth
nv set interface lo ip address 10.20.11.11/32
nv set interface lo type loopback
nv set interface swp1-6,61-64 type swp