nv set bridge domain br_default vlan 2002 vni 102002
nv set evpn enable on
nv set mlag backup 10.10.1.3
nv set mlag enable on
nv set mlag init-delay 2
nv set mlag mac-address 44:38:39:BE:EF:34
nv set nve vxlan enable on
nv set nve vxlan mlag shared-address 10.10.1.34
nv set nve vxlan source address 10.10.1.4
nv set router bgp autonomous-system 4201010104
nv set router bgp enable on
nv set router bgp router-id 10.10.1.4
nv set router policy route-map advertiseloopback rule 10 action permit
nv set router policy route-map advertiseloopback rule 10 match interface lo
nv set router vrr enable on
nv set system global anycast-mac 44:38:39:EE:03:04
nv set system hostname leaf04
nv set vrf default router bgp address-family ipv4-unicast enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected route-map advertiseloopback
nv set vrf default router bgp address-family l2vpn-evpn enable on
nv set vrf default router bgp enable on
nv set vrf default router bgp neighbor peerlink.4094 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor peerlink.4094 remote-as external
nv set vrf default router bgp neighbor peerlink.4094 type unnumbered
nv set vrf default router bgp neighbor swp51 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp51 remote-as external
nv set vrf default router bgp neighbor swp51 type unnumbered
nv set vrf default router bgp neighbor swp52 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp52 remote-as external
nv set vrf default router bgp neighbor swp52 type unnumbered
nv set vrf default router bgp neighbor swp53 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp53 remote-as external
nv set vrf default router bgp neighbor swp53 type unnumbered
nv set vrf default router bgp neighbor swp54 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp54 remote-as external
nv set vrf default router bgp neighbor swp54 type unnumbered
nv set vrf default router bgp path-selection multipath aspath-ignore on
nv set vrf tenant1 evpn enable on
nv set vrf tenant1 evpn vni 400100
nv set interface bond1 bond member swp1
nv set interface bond1 bond mlag enable on
nv set interface bond1 bond mlag id 1
nv set interface bond1 bridge domain br_default access 2002
nv set interface bond1 type bond
nv set interface eth0 ip address dhcp
nv set interface eth0 ip vrf mgmt
nv set interface eth0 type eth
nv set interface lo ip address 10.10.1.4/32
nv set interface lo type loopback
nv set interface peerlink bond member swp49
nv set interface peerlink bond member swp50
nv set interface peerlink type peerlink
nv set interface peerlink.4094 base-interface peerlink
nv set interface peerlink.4094 type sub
nv set interface peerlink.4094 vlan 4094
nv set interface swp1,49-54 link state up
nv set interface swp1,40,49-54 type swp
nv set interface vlan200 ip address 10.100.200.104/24
nv set interface vlan200 ip vrr address 10.100.200.1/24
nv set interface vlan200 vlan 200
nv set interface vlan200,2002 ip vrf tenant1
nv set interface vlan200,2002 ip vrr enable on
nv set interface vlan200,2002 ip vrr mac-address 00:00:5e:00:01:00
nv set interface vlan200,2002 ip vrr state up
nv set interface vlan200,2002 type svi
nv set interface vlan2002 ip address 10.200.2.4/24
nv set interface vlan2002 ip vrr address 10.200.2.1/24
nv set interface vlan2002 vlan 2002