nv set bridge domain br_default vlan 1006 vni 101006
nv set evpn enable on
nv set evpn multihoming enable on
nv set nve vxlan enable on
nv set router bgp autonomous-system 4201010212
nv set router bgp enable on
nv set router bgp router-id 10.10.2.12
nv set router policy route-map advertiseloopback rule 10 action permit
nv set router policy route-map advertiseloopback rule 10 match interface lo
nv set router vrr enable on
nv set system hostname leaf12
nv set vrf default router bgp address-family ipv4-unicast enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected enable on
nv set vrf default router bgp address-family ipv4-unicast redistribute connected route-map advertiseloopback
nv set vrf default router bgp address-family l2vpn-evpn enable on
nv set vrf default router bgp enable on
nv set vrf default router bgp neighbor swp51 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp51 remote-as external
nv set vrf default router bgp neighbor swp51 type unnumbered
nv set vrf default router bgp neighbor swp52 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp52 remote-as external
nv set vrf default router bgp neighbor swp52 type unnumbered
nv set vrf default router bgp neighbor swp53 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp53 remote-as external
nv set vrf default router bgp neighbor swp53 type unnumbered
nv set vrf default router bgp neighbor swp54 address-family l2vpn-evpn enable on
nv set vrf default router bgp neighbor swp54 remote-as external
nv set vrf default router bgp neighbor swp54 type unnumbered
nv set vrf default router bgp path-selection multipath aspath-ignore on
nv set vrf tenant1 evpn enable on
nv set vrf tenant1 evpn vni 400100
nv set interface bond1 bond member swp1
nv set interface bond1 bridge domain br_default access 1006
nv set interface bond1 evpn multihoming segment enable on
nv set interface bond1 evpn multihoming segment local-id 1
nv set interface bond1 evpn multihoming segment mac-address 44:38:39:10:06:01
nv set interface bond1 type bond
nv set interface eth0 ip address dhcp
nv set interface eth0 ip vrf mgmt
nv set interface eth0 type eth
nv set interface lo ip address 10.10.2.12/32
nv set interface lo type loopback
nv set interface swp51-54 type swp
nv set interface vlan1006 ip address 10.112.6.12/24
nv set interface vlan1006 ip vrf tenant1
nv set interface vlan1006 ip vrr address 10.112.6.1/24
nv set interface vlan1006 ip vrr enable on
nv set interface vlan1006 ip vrr mac-address 00:00:5e:00:01:00
nv set interface vlan1006 ip vrr state up
nv set interface vlan1006 type svi
nv set interface vlan1006 vlan 1006